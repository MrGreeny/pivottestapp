﻿using System;
using System.Collections.ObjectModel;

namespace DynamicPivotTestApp
{

    public class Utility
    {

        private static MainViewModel viewModel = null;

        /// <summary>
        /// A static ViewModel used by the views to bind against.
        /// </summary>
        /// <returns>The MainViewModel object.</returns>
        public static MainViewModel ViewModel
        {
            get
            {
                // Delay creation of the view model until necessary
                if (viewModel == null)
                    viewModel = new MainViewModel();

                return viewModel;
            }
        }
    }

    public class PivotSection
    {
        public string Name { get; set; }
        public string SystemLabel { get; set; }
        public ObservableCollection<PivotGroup> Result { get; set; }

        public PivotSection()
        {
            Result = new ObservableCollection<PivotGroup>();
        }
    }


    public class PivotGroup
    {
        public string Title { get; set; }
        public ObservableCollection<string> MediaElementId { get; set; }

        public PivotGroup()
        {
            MediaElementId = new ObservableCollection<string>();
        }
    }

    public class MainViewModel
    {
        public ObservableCollection<PivotSection> PivotItems { get; set; }
        public MainViewModel()
        {
            PivotItems = new ObservableCollection<PivotSection>();

            //load pivot headers
            LoadHeaders();
            LoadData();
        }


        private void LoadHeaders()
        {
            for (int i = 0; i < 5; i++)
            {
                PivotSection pivotItem = new PivotSection
                {
                    Name = "Item Header",
                };
                PivotItems.Add(pivotItem);
            }
        }

        private void LoadData()
        {
            foreach (PivotSection pivotItem in PivotItems)
            {
                pivotItem.Result.Clear();
                for (int i = 0; i < 3; i++)
                {
                    PivotGroup pg = new PivotGroup();
                    pg.Title = "TitleGroup";

                    for (int j = 0; j < 5; j++)
                    {
                        string elementId = "elementID";
                        pg.MediaElementId.Add(elementId);
                    }

                    pivotItem.Result.Add(pg);
                }
            }
        }
    }
}