﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace DynamicPivotTestApp
{
    public  partial class CustomControl : UserControl
    {
        public CustomControl()
        {
            this.InitializeComponent();
        }



        public string TextBlockContentProperty
        {
            get { return (string)GetValue(TextBlockContentPropertyProperty); }
            set { SetValue(TextBlockContentPropertyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextBlockContentProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextBlockContentPropertyProperty =
            DependencyProperty.Register("TextBlockContentProperty", typeof(string), typeof(CustomControl), 
                new PropertyMetadata(0, new PropertyChangedCallback(OnTextChanged)) );

        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CustomControl cc = d as CustomControl;
            string content = (string)e.NewValue;

            cc.TextBlockContent = content;
        }

        private static void OnTextBlockContentPropertyChanged( DependencyObject source , DependencyPropertyChangedEventArgs e)
        {
            CustomControl cc = source as CustomControl;
            string content = (string)e.NewValue;

            cc.TextBlockContent = content;
        }


        //public static readonly DependencyProperty TestProperty =
        //    DependencyProperty.Register(
        //        "Test", typeof(string), typeof(CustomControl), null
        //        );


        //public string Test
        //{
        //    get { return (string)GetValue(TestProperty);  }
        //    set { SetValue(TestProperty, value); }
        //}


        public string TextBlockContent
        {
            get
            {
                return tb.Text;
            }

            set
            {
                tb.Text = value;
                Debug.WriteLine(value);
            }
        }
    }
}
